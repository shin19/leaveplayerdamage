package net.relsn.plugin;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shin19 on 2014/08/21.
 */
public class LeavePlayerDamage extends JavaPlugin {

    static int ConfDistance;
    static boolean ConfStartNotify;
    static boolean ConfStopNotify;
    static boolean ConfDamageFlag;
    static double ConfDamageAmount;
    static boolean ConfWarning;

    @Override
    public void onEnable() {
        getLogger().info(getName() + " is Enabled");

        // loadConfig
        this.saveDefaultConfig();
        ConfDistance = LeavePlayerDamage.this.getConfig().getInt("distance", 100);
        ConfStartNotify = LeavePlayerDamage.this.getConfig().getBoolean("startnotify", true);
        ConfStopNotify = LeavePlayerDamage.this.getConfig().getBoolean("stopnotify", true);
        ConfDamageFlag = LeavePlayerDamage.this.getConfig().getBoolean("givendamage", true);
        ConfDamageAmount = LeavePlayerDamage.this.getConfig().getDouble("damageamount", 1.0);
        ConfWarning = LeavePlayerDamage.this.getConfig().getBoolean("warning", true);

        getCommand("leaveplayerdamage").setExecutor(new LeavePlayerDamageCmd(this));
    }

    @Override
    public void onDisable() {
        getLogger().info(getName() + " is Disabled");
    }

    @Override
    public List<String> onTabComplete(
            CommandSender sender, Command command, String alias, String[] args) {

        if ( args.length == 1 ) {
            String pre = args[0];
            ArrayList<String> candidates = new ArrayList<String>();
            for ( String com : new String[]{"start", "stop", "set", "setrandom", "status", "conf"} ) {
                if ( com.startsWith(pre) && (sender.hasPermission("leaveplayerdamage." + com )|| sender.isOp()) ) {
                    candidates.add(com);
                }
            }
            return candidates;
        }
        if (args.length == 2) {
            String pre = args[1];
            ArrayList<String> candidates = new ArrayList<String>();
            if (args[1].equalsIgnoreCase("conf")) {
                for (String com : new String[]{"distance", "startnotify", "stopnotify", "givendamage", "damageamount", "warning"}) {
                    if (com.startsWith(pre) && sender.hasPermission("leaveplayerdamage.conf." + com)) {
                        candidates.add(com);
                    }
                }
                return candidates;
            }

        }

        return null;
    }

    /**
    @Override
    public List<String> onTabComplete(
            CommandSender sender, Command command, String alias, String[] args) {

        if ( args.length == 1 ) {
            String pre = args[0];
            ArrayList<String> candidates = new ArrayList<String>();
            for ( String com : new String[]{"get", "give", "reload"} ) {
                if ( com.startsWith(pre) && sender.hasPermission("stinger." + com) ) {
                    candidates.add(com);
                }
            }
            return candidates;
        }

        return null;
    }
    */

}
