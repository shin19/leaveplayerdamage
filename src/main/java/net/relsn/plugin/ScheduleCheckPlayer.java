package net.relsn.plugin;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by shin19 on 2014/08/21.
 */
public class ScheduleCheckPlayer extends BukkitRunnable {


    private int Counter = 0;
    private int WaitCounter = 0;
    @Override
    public void run() {
        for (Player p : Bukkit.getOnlinePlayers()) {
            if (LeavePlayerDamageCmd.CheckPlayer.getWorld() != p.getWorld() || LeavePlayerDamageCmd.CheckPlayer == p || p.isDead()) {
                continue;
            } else {
                Location checkplayerloc = LeavePlayerDamageCmd.CheckPlayer.getLocation();
                Location ploc = p.getLocation();
                Location difference = new Location(p.getWorld(), difference(checkplayerloc.getX(), ploc.getX()), difference(checkplayerloc.getY(), ploc.getY()), difference(checkplayerloc.getZ(), ploc.getZ()));
                if (difference.getX() > LeavePlayerDamage.ConfDistance || difference.getY() > LeavePlayerDamage.ConfDistance || difference.getZ() > LeavePlayerDamage.ConfDistance) {
                    WaitCounter = 0;
                    if (Counter == 0) {
                        if (LeavePlayerDamage.ConfWarning) {
                            p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "[Warning]" + ChatColor.RED + " 特定プレイヤーから離れすぎています！");
                        }
                    }
                    if (LeavePlayerDamage.ConfDamageFlag) {
                        p.damage(LeavePlayerDamage.ConfDamageAmount);
                    }
                    Counter++;
                    if (Counter > 10) {
                        Counter = 0;
                    }
                } else {
                    WaitCounter++;
                    if (WaitCounter > 3) {
                        Counter = 0;
                    }
                }
            }
        }
    }

    private double difference(double first, double second) {
        if (first > second) {
            return first - second;
        } else {
            if (first < second) {
                return second - first;
            } else {
                return 0.0;
            }
        }
    }
}
