package net.relsn.plugin;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by shin19 on 2014/08/21.
 */
public class LeavePlayerDamageCmd implements CommandExecutor {
    public LeavePlayerDamage plugin;
    public LeavePlayerDamageCmd(LeavePlayerDamage plugin) {
        this.plugin = plugin;
    }
    public static Player CheckPlayer = null;
    private boolean TaskSet = false;
    /**
     * タブキー補完を実行したときに呼び出されるメソッド
     * @see org.bukkit.plugin.java.JavaPlugin#onTabComplete(org.bukkit.command.CommandSender, org.bukkit.command.Command, java.lang.String, java.lang.String[])
     */




    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("leaveplayerdamage")) {
            /* /lpd 実行時処理 */
            if (args.length < 1) {
                /* 引数が0より少ない場合の処理 */
                sender.sendMessage("Args too few... Try type /lpd help");
                return true;
            } else {
                if (args[0].equalsIgnoreCase("help")) {
                    /* /lpd help 実行時処理 */
                    if (args.length == 1) {
                        /* ページ引数がない場合の処理 */
                        sendHelp(sender, 1);
                        return true;
                    } else {
                        if (args.length == 2) {
                            /* ページ引数がある場合の処理 */
                            if (args[1].equals("1")) {
                                /* /lpd help 1 実行時処理 */
                                sendHelp(sender, 1);
                                return true;
                            } else if (args[1].equals("2")) {
                                sendHelp(sender, 2);
                                return true;
                            } else {
                                /* それ以外の処理 */
                                sender.sendMessage("引数がおかしいです！");
                                return true;
                            }
                        } else {
                            /* 引数が3つ以上の場合の処理 */
                            sender.sendMessage("引数がおかしいです！");
                            return true;
                        }
                    }
                } else if (args[0].equalsIgnoreCase("start")) {
                    /* /lpd start 実行時処理 */
                    if (CheckPlayer == null) {
                        /* ユーザー指定されていない場合の処理 */
                        sender.sendMessage("対象プレイヤーの設定が終わっていません！\n/lpd set <PlayerID> か /lpd setrandom で設定してください！");
                        return true;
                    } else {
                        /* ユーザー指定されている場合の処理 */
                        if (!TaskSet) {
                            if (LeavePlayerDamage.ConfStartNotify) {
                                /* 全体への通知がtrueの場合の処理 */
                                Bukkit.broadcastMessage(ChatColor.LIGHT_PURPLE + CheckPlayer.getDisplayName() + ChatColor.RESET + "から" + LeavePlayerDamage.ConfDistance + "ブロック離れるとダメージを受けます。");
                            } else {
                                /* 全体への通知がfalseの場合の処理 */
                                sender.sendMessage(ChatColor.LIGHT_PURPLE + CheckPlayer.getDisplayName() + ChatColor.RESET + "から" + LeavePlayerDamage.ConfDistance + "ブロック離れるとダメージを受けます。");
                            }
                            /* スケジュール登録 */
                            Bukkit.getScheduler().runTaskTimer(plugin, new ScheduleCheckPlayer(), 10, 30);
                        } else {
                            sender.sendMessage("すでに開始しています。");
                            return true;
                        }
                        TaskSet = true;
                        return true;
                    }
                } else if (args[0].equalsIgnoreCase("stop")) {
                    /* /lpd stop 実行時処理 */
                    TaskSet = false;
                    Bukkit.getScheduler().cancelTasks(plugin);
                    if (LeavePlayerDamage.ConfStopNotify) {
                        Bukkit.broadcastMessage("判定を停止しました");
                    } else {
                        sender.sendMessage("タスクを停止しました。");
                    }
                    return true;
                } else if (args[0].equalsIgnoreCase("set")) {
                    /* /lpd set 実行時処理 */
                    if (args.length < 2 || args.length > 2) {
                        /* 引数が1つ以下か3つ以上の場合の処理 */
                        sender.sendMessage("引数がおかしいです！");
                        return true;
                    } else {
                        /* 引数が2つの場合の処理 */
                        Player player = checkPlayer(args[1]);
                        if (player != null) {
                            /* 判定元プレイヤーがオンラインの場合の処理 */
                            CheckPlayer = player;
                            sender.sendMessage(ChatColor.LIGHT_PURPLE + args[1] + ChatColor.RESET + "を判定元プレイヤーにセットしました。");
                            return true;
                        } else {
                            /* 判定元プレイヤーがオフライン又は存在しない場合の処理 */
                            CheckPlayer = null;
                            sender.sendMessage(ChatColor.LIGHT_PURPLE + args[1] + ChatColor.RESET + "は現在オンラインではないか存在しません。");
                            return true;
                        }
                    }
                } else if (args[0].equalsIgnoreCase("setrandom")) {
                    /* /lpd setrandom 実行時処理 */
                    ArrayList<Player> players = new ArrayList<>();
                    for (Player p : Bukkit.getOnlinePlayers()) {
                        players.add(p);
                    }
                    /* リストをシャッフル */
                    Collections.shuffle(players);
                    for (Player p : players) {
                        CheckPlayer = p;
                        sender.sendMessage(ChatColor.LIGHT_PURPLE + p.getName() + ChatColor.RESET + "を対象にセットしました。");
                        break;
                    }
                    return true;
                } else if (args[0].equalsIgnoreCase("status")) {
                    /* /lpd status 実行時処理 */
                    sender.sendMessage(ChatColor.GOLD + "-------- LeavePlayerDamageプラグイン設定状況 --------");
                    if (TaskSet) {
                        sender.sendMessage("状態 :" + ChatColor.AQUA + " 開始");
                    } else {
                        sender.sendMessage("状態 :" + ChatColor.AQUA + " 停止");
                    }
                    if (CheckPlayer != null) {
                        sender.sendMessage("判定元プレイヤー : " + ChatColor.AQUA + CheckPlayer.getName());
                    } else {
                        sender.sendMessage("判定元プレイヤー : " + ChatColor.AQUA + null);
                    }
                    sender.sendMessage("距離 : " + ChatColor.AQUA + LeavePlayerDamage.ConfDistance);
                    sender.sendMessage("ダメージフラグ : " + ChatColor.AQUA + LeavePlayerDamage.ConfDamageFlag);
                    sender.sendMessage("与ダメージ : " + ChatColor.AQUA + LeavePlayerDamage.ConfDamageAmount + ChatColor.RESET + " (" + ChatColor.GOLD + "ハート" + (LeavePlayerDamage.ConfDamageAmount / 2) + "個分" + ChatColor.RESET + ")");
                    sender.sendMessage("全体開始通知 : " + ChatColor.AQUA + LeavePlayerDamage.ConfStartNotify);
                    sender.sendMessage("エリア外警告 : " + ChatColor.AQUA + LeavePlayerDamage.ConfWarning);
                    return true;
                } else  if (args[0].equalsIgnoreCase("conf")) {
                    if (args.length < 2 || args.length > 3) {
                        sender.sendMessage("引数がおかしいです！");
                        return true;
                    }
                    if (args[1].equalsIgnoreCase("reload")) {
                        this.plugin.reloadConfig();
                        sender.sendMessage("Configをリロードしました。");
                        return true;
                    }
                    if (args[1].equalsIgnoreCase("distance")) {
                        if (args.length == 2) {
                            sender.sendMessage("引数がおかしいです！");
                            return true;
                        } else {
                            int distance;
                            try {
                                distance = Integer.parseInt(args[2]);
                            } catch (NumberFormatException e) {
                                sender.sendMessage(args[2] + "は数字ではありません！ (" + e.getMessage() + ")");
                                return true;
                            }
                            this.plugin.getConfig().set("distance", distance);
                            this.plugin.saveConfig();
                            LeavePlayerDamage.ConfDistance = distance;
                            sender.sendMessage(args[1] + "を" + args[2] + "に設定しました。");
                            return true;
                        }
                    }
                    if (args[1].equalsIgnoreCase("startnotify")) {
                        if (args.length == 2) {
                            sender.sendMessage("引数がおかしいです！");
                            return true;
                        } else {
                            if (args[2].equalsIgnoreCase("true")) {
                                this.plugin.getConfig().set("startnotify", true);
                                this.plugin.saveConfig();
                                LeavePlayerDamage.ConfStartNotify = true;
                                sender.sendMessage(args[1] + "をtrueに設定しました。");
                                return true;
                            } else if (args[2].equalsIgnoreCase("false")) {
                                this.plugin.getConfig().set("startnotify", false);
                                this.plugin.saveConfig();
                                LeavePlayerDamage.ConfStartNotify = false;
                                sender.sendMessage(args[1] + "をfalseに設定しました。");
                                return true;
                            } else {
                                sender.sendMessage(args[2] + "はboolean型ではありません！" + ChatColor.AQUA + "true" + ChatColor.RESET + "か" + ChatColor.AQUA + "false" + ChatColor.RESET + "を入力してください！");
                                return true;
                            }
                        }
                    }
                    if (args[1].equalsIgnoreCase("stopnotify")) {
                        if (args.length == 2) {
                            sender.sendMessage("引数がおかしいです！");
                            return true;
                        } else {
                            if (args[2].equalsIgnoreCase("true")) {
                                this.plugin.getConfig().set("stopnotify", true);
                                this.plugin.saveConfig();
                                LeavePlayerDamage.ConfStopNotify = true;
                                sender.sendMessage(args[1] + "をtrueに設定しました。");
                                return true;
                            } else if (args[2].equalsIgnoreCase("false")) {
                                this.plugin.getConfig().set("stopnotify", false);
                                this.plugin.saveConfig();
                                LeavePlayerDamage.ConfStopNotify = false;
                                sender.sendMessage(args[1] + "をfalseに設定しました。");
                                return true;
                            } else {
                                sender.sendMessage(args[2] + "はboolean型ではありません！" + ChatColor.AQUA + "true" + ChatColor.RESET + "か" + ChatColor.AQUA + "false" + ChatColor.RESET + "を入力してください！");
                                return true;
                            }
                        }
                    }
                    if (args[1].equalsIgnoreCase("givendamage")) {
                        if (args.length == 2) {
                            sender.sendMessage("引数がおかしいです！");
                            return true;
                        } else {
                            if (args[2].equalsIgnoreCase("true")) {
                                this.plugin.getConfig().set("givendamage", true);
                                this.plugin.saveConfig();
                                LeavePlayerDamage.ConfDamageFlag = true;
                                sender.sendMessage(args[1] + "をtrueに設定しました。");
                                return true;
                            } else if (args[2].equalsIgnoreCase("false")) {
                                this.plugin.getConfig().set("givendamage", false);
                                this.plugin.saveConfig();
                                LeavePlayerDamage.ConfDamageFlag = false;
                                sender.sendMessage(args[1] + "をfalseに設定しました。");
                                return true;
                            } else {
                                sender.sendMessage(args[2] + "はboolean型ではありません！" + ChatColor.AQUA + "true" + ChatColor.RESET + "か" + ChatColor.AQUA + "false" + ChatColor.RESET + "を入力してください！");
                                return true;
                            }
                        }
                    }
                    if (args[1].equalsIgnoreCase("damageamount")) {
                        if (args.length == 2) {
                            sender.sendMessage("引数がおかしいです！");
                            return true;
                        } else {
                            double damageamount;
                            try {
                                damageamount = Double.parseDouble(args[2]);
                            } catch (NumberFormatException e) {
                                sender.sendMessage(args[2] + "は実数ではありません！ (" + e.getMessage() + ")");
                                return true;
                            }
                            this.plugin.getConfig().set("damageamount", damageamount);
                            this.plugin.saveConfig();
                            LeavePlayerDamage.ConfDamageAmount = damageamount;
                            sender.sendMessage(args[1] + "を" + args[2] + "に設定しました。");
                            return true;
                        }
                    }
                    if (args[1].equalsIgnoreCase("warning")) {
                        if (args.length == 2) {
                            sender.sendMessage("引数がおかしいです！");
                            return true;
                        } else {
                            if (args[2].equalsIgnoreCase("true")) {
                                this.plugin.getConfig().set("warning", true);
                                this.plugin.saveConfig();
                                LeavePlayerDamage.ConfWarning = true;
                                sender.sendMessage(args[1] + "をtrueに設定しました。");
                                return true;
                            } else if (args[2].equalsIgnoreCase("false")) {
                                this.plugin.getConfig().set("warning", false);
                                this.plugin.saveConfig();
                                LeavePlayerDamage.ConfWarning = false;
                                sender.sendMessage(args[1] + "をfalseに設定しました。");
                                return true;
                            } else {
                                sender.sendMessage(args[2] + "はboolean型ではありません！" + ChatColor.AQUA + "true" + ChatColor.RESET + "か" + ChatColor.AQUA + "false" + ChatColor.RESET + "を入力してください！");
                                return true;
                            }
                        }
                    }
                } else {
                    /* 引数一つ目のコマンドが存在しない場合の処理 */
                    sender.sendMessage("引数がおかしいです！");
                    return true;
                }
            }
        }
        return true;
    }
    public void sendHelp(CommandSender sender, int help) {
        if (help == 1) {
            sender.sendMessage(ChatColor.GOLD + "-------------------- LeavePlayerDamage Help (1/2) --------------------");
            sender.sendMessage("/lpd start          : プラグインの処理を開始します。");
            sender.sendMessage("/lpd stop           : プラグインの処理を終了します。");
            sender.sendMessage("/lpd set <PlayerID> : 判定元プレイヤーをセットします。");
            sender.sendMessage("/lpd setrandom      : 判定元プレイヤーを無作為に選出します。");
            sender.sendMessage("/lpd help <page>    : コマンドヘルプを表示します。");
            sender.sendMessage("/lpd status         : 現在の設定を表示します。");
            return;
        } else if (help == 2) {
            sender.sendMessage(ChatColor.GOLD + "-------------------- LeavePlayerDamage Help (2/2) --------------------");
            sender.sendMessage(ChatColor.GOLD + "----------------------------- Config変更 -----------------------------");
            sender.sendMessage("/lpd conf distance <amount>         : エリア外判定の距離。[整数]");
            sender.sendMessage("/lpd conf startnotify <true/false>  : タスク開始時にサーバー全体に通知するかどうか。[true/false]");
            sender.sendMessage("/lpd conf stopnotify <true/false>   : タスク停止時にサーバー全体に通知するかどうか。[true/false]");
            sender.sendMessage("/lpd conf givendamage <true/false>  : エリア外のプレイヤーにダメージを与えるかどうか。[true/false]");
            sender.sendMessage("/lpd conf damageamount <amount>     : エリア外のプレイヤーに与えるダメージ。[実数]");
            sender.sendMessage("/lpd conf warning <true/false>      : エリア外のプレイヤーに警告を出すかどうか。[true/false]");
            sender.sendMessage("/lpd conf reload                    : config.ymlをリロードする。");
            return;
        } else {
            sender.sendMessage("ありえない領域に到達しました。鯖管理者又はプラグイン製作者にご連絡ください。");
            Logger logger = Logger.getLogger("Minecrart");
            logger.warning("例外発生: LeavePlayerDamageCmd sendHelp内");
            logger.warning("shin19までサーバーログとともにご連絡いただければ幸いです。\n連絡先はLeavePlayerDamage.jar内'contact.txt'に記載してあります。");
            return;
        }
    }

    private Player checkPlayer(String name) {
        for (Player p : Bukkit.getOnlinePlayers()) {
            if (p.getName().equalsIgnoreCase(name)) {
                return p;
            }
        }
        return null;
    }





}
